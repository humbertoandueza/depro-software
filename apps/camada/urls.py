from django.conf.urls import url
from .views import *

urlpatterns = [
    #usuario
    url(r'^create/usuario/$', CreateUsuario.as_view(),name="usuario_create"),
    url(r'^list/usuario/$', ListUsuario.as_view(),name="usuario_list"),
    url(r'^perfil/$', UpdatePerfil.as_view(),name="perfil"),
    
    #formula
    url(r'^formula/$', UpdateFormula.as_view(),name="formula"),



    #productor
    url(r'^create/productor/$', CreateProductor.as_view(),name="productor_create"),
    url(r'^list/productor/$', ListProductor.as_view(),name="productor_list"),
    url(r'^update/productor/(?P<pk>\d+)/$', UpdateProductor.as_view(),name="productor_update"),

	#especies
    url(r'^create/especie/$', CreateEspecie.as_view(),name="especie_create"),
    url(r'^list/especie/$', ListEspecie.as_view(),name="especie_list"),
    url(r'^update/especie/(?P<pk>\d+)/$', UpdateEspecie.as_view(),name="especie_update"),
    url(r'^delete/especie/(?P<pk>\d+)/$', DeleteEspecie.as_view(),name="especie_delete"),

    #alimentos
    url(r'^create/alimento/$', CreateAlimento.as_view(),name="alimento_create"),
    url(r'^list/alimento/$', ListAlimento.as_view(),name="alimento_list"),
    url(r'^update/alimento/(?P<pk>\d+)/$', UpdateAlimento.as_view(),name="alimento_update"),
    url(r'^delete/alimento/(?P<pk>\d+)/$', DeleteAlimento.as_view(),name="alimento_delete"),

    #estanque
    url(r'^create/estanque/$', CreateEstanque.as_view(),name="estanque_create"),
    url(r'^list/estanque/$', ListEstanque.as_view(),name="estanque_list"),
    url(r'^update/estanque/(?P<pk>\d+)/$', UpdateEstanque.as_view(),name="estanque_update"),
    url(r'^list/estanque_m/$', ListEstanque_m.as_view(),name="estanque_list_m"),
    url(r'^update/estanque_m/(?P<pk>\d+)/$', UpdateEstanque_m.as_view(),name="estanque_update_m"),

    url(r'^delete/estanque/(?P<pk>\d+)/$', DeleteEstanque.as_view(),name="estanque_delete"),


    #camada 
    url(r'^create/camada/$', CreateCamada.as_view(),name="camada_create"),
    url(r'^list/camada/$', ListCamada.as_view(),name="camada_list"),
    url(r'^list_g/camada/$', ListCamada_g.as_view(),name="camada_list_g"),
    url(r'^list_l/camada/$', ListCamada_l.as_view(),name="camada_list_l"),
    url(r'^list_p/camada/$', ListCamada_p.as_view(),name="camada_list_p"),
    url(r'^update/camada/(?P<pk>\d+)/$', UpdateCamada.as_view(),name="camada_update"),
    url(r'^delete/camada/(?P<pk>\d+)/$', DeleteCamada.as_view(),name="camada_delete"),




    #url(r'^panel/$', Panel.as_view(),name="panel"),

]

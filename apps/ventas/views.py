from django.shortcuts import render,get_object_or_404,redirect
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.views.generic.list import ListView
from apps.carton.cart import Cart
from django.core.urlresolvers import reverse_lazy
from apps.core.mixins import ProductorMixinRequired,LoginRequiredMixin,SuperUserMixinRequired
from .models import *
from apps.alevinaje.models import Alevinaje
from apps.camada.models import Especie,Estanque,Camada

from apps.core.views import Notificaciones,UpdateNotificacion
# Create your views here.
class ListVentas(SuperUserMixinRequired,ListView):
	model = Venta
	template_name = 'panel/ventas/ventas/ventas_prod_list.html'

class ListVentasProductor(ProductorMixinRequired,TemplateView):
	template_name = 'panel/ventas/ventas/ventas_prod_list.html'
	def get(self,request,*args,**kwargs):
		data = Venta.objects.filter(productor=request.user)
		return render(request,self.template_name,{'object_list':data})

class ListAlevinaje(ProductorMixinRequired,TemplateView):
	template_name = 'panel/ventas/alevines/alevines_list.html'
	def get(self,request):
		data = Alevinaje.objects.filter(estatus=3,vendido=False).order_by('-id')
		return render(request,self.template_name,{'object_list':data})

	def post(self,request):
		cart = Cart(request.session)
		alevin = Especie.objects.get(pk=int(request.POST['alevin']))
		cantidad = int(request.POST['cantidad'])
		cart.add(alevin, price=alevin.precio,quantity=cantidad,id_a=(request.POST['id']))
		return redirect(reverse_lazy('ventas:alevines_list'))

class RemoveCarrito(ProductorMixinRequired,TemplateView):
	def get(self,request):
		cart = Cart(request.session)
		if request.GET:
			alevin = Especie.objects.get(pk=int(request.GET['id']))
			cart.remove(alevin)
		else:
			cart.clear()
		return redirect(reverse_lazy('ventas:alevines_list'))


class RealizarPedido(ProductorMixinRequired,TemplateView):
	def get(self,request):
		cart = Cart(request.session)
		if cart.items:
			venta = Venta(productor=request.user)
			venta.save()
			for i in cart.items:
				ale = Alevinaje.objects.get(id=i.id_a)
				detalle = VentaDetalle(venta=Venta.objects.last(),especie=ale,precio=i.product.precio,cantidad=i.quantity,subtotal=i.subtotal)
				detalle.save()
				resta = int(ale.cantidad)-int(i.quantity)
				Alevinaje.objects.filter(id=i.id_a).update(cantidad=resta)

			Venta.objects.filter(id=venta.id).update(total=cart.total)
			Notificaciones(usuario=request.user,contenido="Solicitud de compra de alevines",tipo="productor",url=venta.id)
			Notificaciones(usuario=request.user,contenido="Pedido en espera, se le notificará al revisar su solicitud",tipo="administrador",url=venta.id)
			cart.clear()
		return  redirect(reverse_lazy('ventas:alevines_list'))


class DetailVenta(LoginRequiredMixin,TemplateView):
	template_name = 'panel/ventas/alevines/alevines_detail.html'
	def get(self,request,pk):
		if request.GET:
			UpdateNotificacion(id=int(request.GET['noti']))
		venta = Venta.objects.get(pk=pk)
		detalles = VentaDetalle.objects.filter(venta=pk)
		return render(request,self.template_name,{'venta':venta,'detalles':detalles})

	def post(self,request,pk):
		venta = Venta.objects.get(pk=pk)
		detalles = VentaDetalle.objects.filter(venta=pk)
		if len(request.POST)>1:
			print(request.POST['acepto'])
			for i in detalles:
				if i.especie.cantidad == 0:
					Alevinaje.objects.filter(id=i.especie.id).update(vendido=True)
					Estanque.objects.filter(id=i.especie.estanque.id).update(estatus="Mantenimiento")

			Venta.objects.filter(pk=pk).update(estatus="Procesada")
			Notificaciones(usuario=venta.productor,contenido="Su pedido a sido procesado con exito",tipo="administrador",url=pk)
		else:
			for i in detalles:
				alevinaje = Alevinaje.objects.get(id=i.especie.id)
				suma = int(alevinaje.cantidad)+int(i.cantidad)
				Alevinaje.objects.filter(id=i.especie.id).update(cantidad=suma)
			Venta.objects.filter(pk=pk).update(estatus="Rechazada")
			Notificaciones(usuario=venta.productor,contenido="Su pedido a sido rechazado",tipo="administrador",url=pk)
		return redirect('ventas:alevines_detail',pk=venta.id)

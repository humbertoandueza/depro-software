from django.conf.urls import url
from .views import *

urlpatterns = [


    url(r'^estadistica/$', EstadisticaDetail.as_view(),name="estadistica_detail"),
    url(r'^estadistica_especie/$', EstadisticaEspecie.as_view(),name="estadistica_especie"),
    url(r'^estadistica_estado/$', EstadisticaEstado.as_view(),name="estadistica_estado"),
    url(r'^alimento/$', AliementoPDF,name="alimento"),
    url(r'^especie/$', EspeciePDF,name="especie"),
    url(r'^productor/$', ProductorPDF,name="productor"),
    url(r'^camada_f/$', Camada_fPDF,name="camada_f"),
    url(r'^camada_g/$', Camada_gPDF,name="camada_g"),
    url(r'^camada_l/$', Camada_lPDF,name="camada_l"),
    url(r'^camada_p/$', Camada_pPDF,name="camada_p"),








    #url(r'^panel/$', Panel.as_view(),name="panel"),

]

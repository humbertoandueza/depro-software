from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.shortcuts import redirect

#Clases para Acceso a niveles de usuario:

#clase para el LoginRequired
class LoginRequiredMixin(object):

    @classmethod
    def as_view(cls):
        return login_required(super(LoginRequiredMixin, cls).as_view())


#clase para acceso del superusuario
class SuperUserMixinRequired(LoginRequiredMixin,object):
	"""Nivel de usuario super User"""
	def dispatch(self,request,*args,**kwargs):
		print (request.user.is_administrador)
		if not request.user.is_administrador:
			return redirect(reverse_lazy('core:panel'))
		return super(SuperUserMixinRequired,self).dispatch(request,*args,**kwargs)


#clase para acceso de staff
class ProductorMixinRequired(LoginRequiredMixin,object):
	"""Nivel de usuario super User"""
	def dispatch(self,request,*args,**kwargs):
		print (request.user.is_productor)
		if not request.user.is_productor:
			return redirect(reverse_lazy('core:panel'))
		return super(ProductorMixinRequired,self).dispatch(request,*args,**kwargs)
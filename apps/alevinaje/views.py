from django.shortcuts import render,get_object_or_404,redirect
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse_lazy
from apps.core.mixins import SuperUserMixinRequired

from .models import *
from .forms import *
from apps.camada.models import Estanque,Camada

#crud Vitamina
class CreateVitamina(SuperUserMixinRequired,CreateView):
	model = Vitamina
	form_class = VitaminaForm
	template_name = 'panel/alevinaje/vitamina/vitamina_form.html'
	success_url = reverse_lazy('alevinaje:vitamina_list')


class ListVitamina(SuperUserMixinRequired,ListView):
    model = Vitamina
    paginate_by = 50
    template_name = 'panel/alevinaje/vitamina/vitamina_list.html'
    ordering = ['-id']

class UpdateVitamina(SuperUserMixinRequired,UpdateView):
	model = Vitamina
	form_class = VitaminaForm
	template_name = 'panel/alevinaje/vitamina/vitamina_update.html'
	success_url = reverse_lazy('alevinaje:vitamina_list')

class DeleteVitamina(SuperUserMixinRequired,DeleteView):
	model = Vitamina
	template_name = 'panel/alevinaje/vitamina/vitamina_confirm_delete.html'
	success_url = reverse_lazy('alevinaje:vitamina_list')


#Proceso Alevinaje
class CreateAlevinaje(SuperUserMixinRequired,CreateView):
	model = Alevinaje
	form_class = AlevinajeForm
	template_name = 'panel/alevinaje/alevinaje/alevinaje_form.html'
	success_url = reverse_lazy('alevinaje:alevinaje_list')
	
	def post(self,request, *args, **kwargs):
		form = AlevinajeForm(request.POST)
		if form.is_valid():
			alevinaje = form.save()
			alevinaje.save()
			Estanque.objects.filter(id=alevinaje.estanque.id).update(estatus="Ocupado")
			Camada.objects.filter(id=alevinaje.camada.id).update(estatus=3)
			Alevinaje.objects.filter(id=alevinaje.id).update(fecha=alevinaje.camada.fecha)

			return redirect(self.success_url)
		return render(request,self.template_name,{"form":form})

class ListAlevinaje(SuperUserMixinRequired,TemplateView):
	def get(self,request):
		data = Alevinaje.objects.filter(estatus=2).order_by('-id')
		return render(request,'panel/alevinaje/alevinaje/alevinaje_list.html',{'object_list':data})

class ListAlevinaje_l(SuperUserMixinRequired,TemplateView):
	def get(self,request):
		data = Alevinaje.objects.filter(estatus=3,vendido=False).order_by('-id')
		return render(request,'panel/alevinaje/alevinaje/alevinaje_list_l.html',{'object_list':data})

class ListAlevinaje_p(SuperUserMixinRequired,TemplateView):
	def get(self,request):
		data = Alevinaje.objects.filter(estatus=4).order_by('-id')
		return render(request,'panel/alevinaje/alevinaje/alevinaje_list_p.html',{'object_list':data})

class UpdateAlevinaje(SuperUserMixinRequired,UpdateView):
	model = Alevinaje
	form_class = AlevinajeUpdateForm
	template_name = 'panel/alevinaje/alevinaje/alevinaje_update.html'
	success_url = reverse_lazy('alevinaje:alevinaje_list')

	def post(self,request,pk, *args, **kwargs):
		alevinaje = get_object_or_404(Alevinaje,pk=pk)
		form = AlevinajeUpdateForm(request.POST,instance=alevinaje)
		if form.is_valid():
			alevinaje = form.save()
			alevinaje.save()
			Alevinaje.objects.filter(pk=pk).update(cantidad=alevinaje.camada.cantidad_e)
			if alevinaje.estatus == 'Perdida':
				Estanque.objects.filter(id=alevinaje.estanque.id).update(estatus="Mantenimiento")
			return redirect(self.success_url)
		return render(request,self.template_name,{"form":form})

class DeleteAlevinaje(SuperUserMixinRequired,DeleteView):
	model = Alevinaje
	template_name = 'panel/alevinaje/alevinaje/alevinaje_confirm_delete.html'
	success_url = reverse_lazy('alevinaje:alevinaje_list')
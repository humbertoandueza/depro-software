#!/usr/bin/python
# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from .models import *
from apps.core.models import User

class UserForm(UserCreationForm):
    class Meta:
        model= User
        fields=("cedula","username","first_name","last_name","estados","email")
        

        widgets = {
        'estados':forms.Select(attrs={'class':'form-control','required':'true'}),
        
        }
        error_messages = {
            'cedula': {
                'unique': 'Productor ya existe,Por Favor registre otro.'
            },
            'email':{
                'unique': 'Correo Electronico ya existe, Por favor registre otro.'
            },
            'username':{
                'unique': 'Nombre de usuario ya existe, Por favor registre otro.'
            }
        }

class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('is_active',)


class FormulaUpdateForm(forms.ModelForm):
    class Meta:
        model = Formula
        fields = ('__all__')


class PerfilUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields=("first_name","last_name","estados","email")
        widgets = {
        'estados':forms.Select(attrs={'class':'form-control','required':'true'}),
        
        }
        error_messages = {
            'email':{
                'unique': 'Correo Electronico ya existe, Por favor registre otro.'
            }
        }

class EspecieForm(forms.ModelForm):
    class Meta:
        model= Especie
        fields=("nombre","taza_m","taza_s","precio","ovas")

class AlimentoForm(forms.ModelForm):
    class Meta:
        model= Alimento
        fields=("nombre","tipo","peso")


class EstanqueForm(forms.ModelForm):
    class Meta:
        model= Estanque
        fields=("numero","medida","tipo")

        error_messages = {
            'numero': {
                'unique': 'Este estanque ya existe,Por Favor registre otro.'
            },
        }

class Estanque_mForm(forms.ModelForm):
    class Meta:
        model= Estanque
        fields=("estatus","fecha")
        widgets = {
        'estatus':forms.Select(attrs={'class':'form-control'}),
        
        }

    def __init__(self, *args, **kwargs):
        super(Estanque_mForm, self).__init__(*args, **kwargs)
        self.fields['estatus'].choices = (
            ("Disponible","Disponible"),
            ("Mantenimiento","Mantenimiento"),
        )

class CamadaForm(forms.ModelForm):
    class Meta:
        model= Camada
        fields=("cantidad_h","cantidad_m","cantidad_k","estanque","alimento","especie")

        widgets = {
        'especie':forms.Select(attrs={'class':'form-control'}),
        'alimento':forms.Select(attrs={'class':'form-control'}),
        'estanque':forms.Select(attrs={'class':'form-control'}),
        
        }

    def __init__(self, *args, **kwargs):
        super(CamadaForm, self).__init__(*args, **kwargs)
        self.fields['estanque'].queryset = Estanque.objects.filter(estatus="Disponible")

class CamadaUpdateForm(forms.ModelForm):
    class Meta:
        model= Camada
        fields=("estatus",)

        widgets = {
        'estatus':forms.Select(attrs={'class':'form-control'}),

        }
    def __init__(self, *args, **kwargs):
        super(CamadaUpdateForm, self).__init__(*args, **kwargs)
        self.fields['estatus'].queryset = Estatus.objects.exclude(estatus="Lista")

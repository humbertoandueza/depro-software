from django.shortcuts import render,get_object_or_404,redirect
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse_lazy
from apps.core.mixins import SuperUserMixinRequired,LoginRequiredMixin
from apps.core.models import User
from .models import *
from .forms import *
import datetime

#Crud Usuario
class CreateUsuario(SuperUserMixinRequired,CreateView):
	model = User
	form_class = UserForm
	template_name = 'panel/usuario/usuario_form.html'
	success_url = reverse_lazy('camada:usuario_list')

	def post(self,request, *args, **kwargs):
		form = UserForm(request.POST)
		if form.is_valid():
			usuario= form.save()
			usuario.is_administrador = True
			usuario.save()
			return redirect(self.success_url)
		return render(request,self.template_name,{"form":form})

class ListUsuario(SuperUserMixinRequired,ListView):
    paginate_by = 50
    template_name = 'panel/usuario/usuario_list.html'
    ordering = ['-id']
    def get(self,request):
    	usuario = User.objects.filter(is_administrador=True)
    	return render(request,self.template_name,{'object_list':usuario})

class UpdatePerfil(LoginRequiredMixin,TemplateView):
	template_name = 'panel/usuario/usuario_update.html'
	def get(self,request):
		form = PerfilUpdateForm(instance=request.user)
		return render(request,self.template_name,{'form':form})
	def post(self,request):
		form = PerfilUpdateForm(request.POST,instance=request.user)
		if form.is_valid():
			form.save()
			return redirect('camada:perfil')
		else:
			form = PerfilUpdateForm(instance=request.user)
			return render(request,self.template_name,{'form':form,'error':'El correo ya existe,registre otro'})

#FormulaUpdate
class UpdateFormula(SuperUserMixinRequired,TemplateView):
	template_name = 'panel/camada/camada/formula_update.html'
	def get(self,request):
		formula = Formula.objects.last()
		form = FormulaUpdateForm(instance=formula)
		return render(request,self.template_name,{'form':form})
	def post(self,request):
		formula = Formula.objects.last()
		form = FormulaUpdateForm(request.POST,instance=formula)
		if form.is_valid():
			form.save()
			return redirect('camada:formula')
		else:
			form = FormulaUpdateForm(instance=formula)
			return render(request,self.template_name,{'form':form,'error':'Verifique que el porcentaje sera correcto'})

#Crud Productor
class CreateProductor(SuperUserMixinRequired,CreateView):
	model = User
	form_class = UserForm
	template_name = 'panel/productor/productor_form.html'
	success_url = reverse_lazy('camada:productor_list')

	def post(self,request, *args, **kwargs):
		form = UserForm(request.POST)
		if form.is_valid():
			productor= form.save()
			productor.is_productor = True
			productor.save()
			return redirect(self.success_url)
		return render(request,self.template_name,{"form":form})

class ListProductor(SuperUserMixinRequired,ListView):
    paginate_by = 50
    template_name = 'panel/productor/productor_list.html'
    ordering = ['-id']
    def get(self,request):
    	productor = User.objects.filter(is_productor=True)
    	return render(request,self.template_name,{'object_list':productor})


class UpdateProductor(SuperUserMixinRequired,UpdateView):
	model = User
	form_class = UserUpdateForm
	template_name = 'panel/productor/productor_update.html'
	success_url = reverse_lazy('camada:productor_list')


#Crud especie
class CreateEspecie(SuperUserMixinRequired,CreateView):
	model = Especie
	form_class = EspecieForm
	template_name = 'panel/camada/especie/especie_form.html'
	success_url = reverse_lazy('camada:especie_list')

class ListEspecie(SuperUserMixinRequired,ListView):
    model = Especie
    paginate_by = 50
    template_name = 'panel/camada/especie/especie_list.html'
    ordering = ['-id']

class UpdateEspecie(SuperUserMixinRequired,UpdateView):
	model = Especie
	form_class = EspecieForm
	template_name = 'panel/camada/especie/especie_update.html'
	success_url = reverse_lazy('camada:especie_list')

class DeleteEspecie(SuperUserMixinRequired,DeleteView):
	model = Especie
	template_name = 'panel/camada/especie/especie_confirm_delete.html'
	success_url = reverse_lazy('camada:especie_list')



#crud alimento
class CreateAlimento(SuperUserMixinRequired,CreateView):
	model = Alimento
	form_class = AlimentoForm
	template_name = 'panel/camada/alimento/alimento_form.html'
	success_url = reverse_lazy('camada:alimento_list')

class ListAlimento(SuperUserMixinRequired,ListView):
    model = Alimento
    paginate_by = 50
    template_name = 'panel/camada/alimento/alimento_list.html'
    ordering = ['-id']

class UpdateAlimento(SuperUserMixinRequired,UpdateView):
	model = Alimento
	form_class = AlimentoForm
	template_name = 'panel/camada/alimento/alimento_update.html'
	success_url = reverse_lazy('camada:alimento_list')

class DeleteAlimento(SuperUserMixinRequired,DeleteView):
	model = Alimento
	template_name = 'panel/camada/alimento/alimento_confirm_delete.html'
	success_url = reverse_lazy('camada:alimento_list')



#crud estanque
class CreateEstanque(SuperUserMixinRequired,CreateView):
	model = Estanque
	form_class = EstanqueForm
	template_name = 'panel/camada/estanque/estanque_form.html'
	success_url = reverse_lazy('camada:estanque_list')

	def post(self,request, *args, **kwargs):
		form = EstanqueForm(request.POST)
		if form.is_valid():
			#print(form)
			#form['numero'] = str(request.POST['est'])+'-'+str(request.POST['numero'])
			query = Estanque.objects.filter(tipo=request.POST['tipo'],numero=request.POST['numero'])
			print(query)
			if query:
				error = 'Ya existe un estanque con este número y tipo.'
				return render(request,self.template_name,{"form":form,"errores":error})
			else:
				estanque= form.save()
				estanque.fecha = datetime.datetime.now()
				estanque.save()
				return redirect(self.success_url)
		return render(request,self.template_name,{"form":form})

class ListEstanque(SuperUserMixinRequired,ListView):
    model = Estanque
    paginate_by = 50
    template_name = 'panel/camada/estanque/estanque_list.html'
    ordering = ['-id']

class UpdateEstanque(SuperUserMixinRequired,UpdateView):
	model = Estanque
	form_class = EstanqueForm
	template_name = 'panel/camada/estanque/estanque_update.html'
	success_url = reverse_lazy('camada:estanque_list')
#Estanques en mantenimiento
class ListEstanque_m(SuperUserMixinRequired,TemplateView):
	def get(self,request):
		data = Estanque.objects.filter(estatus="Mantenimiento").order_by('-id')
		return render(request,'panel/camada/estanque/estanque_list_m.html',{'object_list':data})

class UpdateEstanque_m(SuperUserMixinRequired,UpdateView):
	model = Estanque
	form_class = Estanque_mForm
	template_name = 'panel/camada/estanque/estanque_update_m.html'
	success_url = reverse_lazy('camada:estanque_list_m')
	
class DeleteEstanque(SuperUserMixinRequired,DeleteView):
	model = Estanque
	template_name = 'panel/camada/estanque/estanque_confirm_delete.html'
	success_url = reverse_lazy('camada:estanque_list')




#Proceso Camada
class CreateCamada(SuperUserMixinRequired,CreateView):
	model = Camada
	form_class = CamadaForm
	template_name = 'panel/camada/camada/camada_form.html'
	success_url = reverse_lazy('camada:camada_list')

	
	def post(self,request, *args, **kwargs):
		form = CamadaForm(request.POST)
		if form.is_valid():
			camada = form.save()
			camada.save()
			formula = Formula.objects.last().porcentaje  
			camada.cantidad_e = int(camada.cantidad_k)*int(camada.especie.ovas)*formula/100
			camada.save()
			Estanque.objects.filter(id=camada.estanque.id).update(estatus="Ocupado",fecha=datetime.datetime.now())
			return redirect(self.success_url)
		return render(request,self.template_name,{"form":form})

class ListCamada(SuperUserMixinRequired,TemplateView):
	def get(self,request):
		data = Camada.objects.filter(estatus=1).order_by('-id')
		return render(request,'panel/camada/camada/camada_list.html',{'object_list':data})

class ListCamada_g(SuperUserMixinRequired,TemplateView):
	def get(self,request):
		data = Camada.objects.filter(estatus=2).order_by('-id')
		return render(request,'panel/camada/camada/camada_list_g.html',{'object_list':data})

class ListCamada_l(SuperUserMixinRequired,TemplateView):
	def get(self,request):
		data = Camada.objects.filter(estatus=3).order_by('-id')
		return render(request,'panel/camada/camada/camada_list_l.html',{'object_list':data})

class ListCamada_p(SuperUserMixinRequired,TemplateView):
	def get(self,request):
		data = Camada.objects.filter(estatus=4).order_by('-id')
		return render(request,'panel/camada/camada/camada_list_p.html',{'object_list':data})

class UpdateCamada(SuperUserMixinRequired,UpdateView):
	model = Camada
	form_class = CamadaUpdateForm
	template_name = 'panel/camada/camada/camada_update.html'
	success_url = reverse_lazy('camada:camada_list')

	def post(self,request,pk, *args, **kwargs):
		camada = get_object_or_404(Camada,pk=pk)
		form = CamadaUpdateForm(request.POST,instance=camada)
		if form.is_valid():
			camada = form.save()
			camada.save()
			Estanque.objects.filter(id=camada.estanque.id).update(estatus="Mantenimiento",fecha=datetime.datetime.now())
			return redirect(self.success_url)
		return render(request,self.template_name,{"form":form})

class DeleteCamada(SuperUserMixinRequired,DeleteView):
	model = Camada
	template_name = 'panel/camada/camada/camada_confirm_delete.html'
	success_url = reverse_lazy('camada:camada_list')






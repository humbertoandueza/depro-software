from django.shortcuts import render
from django.views.generic.base import TemplateView
from .mixins import LoginRequiredMixin
from .models import Notificacion
# Create your views here.
class Landing(TemplateView):
	def get(self,request):
		return render(request,'landing/index.html')

class Panel(LoginRequiredMixin,TemplateView):
	def get(self,request):
		return render(request,'panel/index.html')

def Notificaciones(usuario,contenido,url,tipo):
	notificacion = Notificacion(usuario=usuario,contenido=contenido,url=url,tipo=tipo)
	notificacion.save()
	return True

def UpdateNotificacion(id):
	notificacion = Notificacion.objects.filter(id=id).update(estatus=True)
	return True

class ListNotificaciones(LoginRequiredMixin,TemplateView):
	def get(self,request):
		if request.user.is_administrador:
			notificacion = Notificacion.objects.filter(tipo="productor").order_by('-id')
		
		if request.user.is_productor:
			notificacion = Notificacion.objects.filter(usuario=request.user,tipo="administrador").order_by('-id')
		return render(request,'panel/notificaciones.html',{'todas_notificaciones':notificacion})
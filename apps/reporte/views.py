from django.shortcuts import render,redirect
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.core import serializers
from django.http import JsonResponse
import os
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.utils import timezone

from io import BytesIO
from reportlab.platypus.tables import Table, TableStyle, CellStyle, LongTable
from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.colors import HexColor
from reportlab.lib.pagesizes import letter, A4,  LETTER, landscape
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import Paragraph, Table, BaseDocTemplate, Frame, PageTemplate, Image, TableStyle, Spacer, SimpleDocTemplate, PageBreak
from reportlab.lib.units import inch, cm, mm 
from reportlab.lib.enums import TA_LEFT, TA_RIGHT, TA_CENTER, TA_JUSTIFY
from datetime import datetime
from django.db.models import Count

from django.contrib.staticfiles import finders

import calendar
import datetime
#import models 
from apps.core.models import User
from apps.alevinaje.models import Alevinaje
from apps.camada.models import *
from apps.ventas.models import *
#from datetime import datetime
class EstadisticaDetail(TemplateView):
	def get(self,request):
		today=datetime.datetime.now()
		if len(str(today.month)) == 1:
			mes = '0'+str(today.month)
		else:
			mes = today.month

		inicio="%s-%s-01" % (today.year, mes)
		fin="%s-%s-%s" % (today.year, mes, calendar.monthrange(today.year-1, int(mes))[1])
		lista = Alevinaje.objects.filter(fecha__range=[inicio,fin],estatus=3).count()

		perdida = Alevinaje.objects.filter(fecha__range=[inicio,fin],estatus=4).count()
		found = "True"
		if lista == 0 and perdida == 0:
			found = "False"
		arr = [{"nombre":"Lista","count":lista},{"nombre":"Perdida","count":perdida},{"found":found}]
		return render(request,'panel/reporte/estadistica.html',{"alevinaje":arr})
	def post(self,request):
		mes = request.POST['mes']
		ano = request.POST['ano']
		inicio="%s-%s-01" % (ano, mes)
		fin="%s-%s-%s" % (int(ano), mes, calendar.monthrange(int(ano)-1, int(mes))[1])
 		
		lista = Alevinaje.objects.filter(fecha__range=[inicio,fin],estatus=3).count()

		perdida = Alevinaje.objects.filter(fecha__range=[inicio,fin],estatus=4).count()
		found = "True"
		if lista == 0 and perdida == 0:
			found = "False"
		arr = [{"nombre":"Lista","count":lista},{"nombre":"Perdida","count":perdida},{"found":found}]
		return JsonResponse(arr,safe=False)

# Número de Página -----------------------------------------------------------------------------
class NumberedCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []
 
    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()
 
    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)
 
    def draw_page_number(self, page_count):
        self.setFont("Helvetica", 7)
        self.drawRightString(25 * mm, 4 * mm + (0.3 * inch),"Pagina %d de %d" % (self._pageNumber, page_count))


# PDF de Alimentos -----------------------------------------------------------------------------
def HeaderFooterAlimento(canvas,doc):
        canvas.saveState()
        title = "Reporte de Alimento"
        canvas.setTitle(title)
        
        Story=[]
        styles = getSampleStyleSheet()
        
        archivo_imagen = finders.find('base/assets/img/cintillo.png')
        canvas.drawImage(archivo_imagen, 30, 720, width=540,height=100,preserveAspectRatio=True)

        fecha = datetime.datetime.now().strftime('%d/%m/%Y ')
        # Estilos de Párrafos
        ta_c = ParagraphStyle('parrafos', 
                            alignment = TA_CENTER,
                            fontSize = 9,
                            fontName="Helvetica",
                            )   
        ta_l = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )
        ta_l7 = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 7,
                            fontName="Helvetica-Bold",
                            )
        ta_r = ParagraphStyle('parrafos', 
                            alignment = TA_RIGHT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )

        # Header
        header = Paragraph("Listado de Alimentos ",ta_l,)
        w,h = header.wrap(doc.width+250, doc.topMargin)
        header.drawOn(canvas, 215, doc.height -10 + doc.topMargin-80)

        #header1 = Paragraph("<u>DIRECCIÓN DE AGRICULTURA Y TIERRA</u> ",ta_r,)
        #w,h = header1.wrap(doc.width-115, doc.topMargin)
        #header1.drawOn(canvas, 140, doc.height -10 + doc.topMargin - 2)
        
        P1 = Paragraph('''N°''',ta_c)
        P2 = Paragraph('''Nombre''',ta_c)
        P3 = Paragraph('''Tipo''',ta_c)
        P4 = Paragraph('''Peso (KG)''',ta_c)
        data = [[P1, P2, P3, P4]]
        header2 = Table(data, colWidths = [35,155,150,80])
        header2.setStyle(TableStyle( 
            [   
                ('GRID', (0, -1), (-1, -1), 1, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), '#28cde0'),
                ('ALIGN', (0, 0), (-1, -3), "CENTER"),
            ]
            ))
        w,h = header2.wrap(doc.width-115, doc.topMargin)
        header2.drawOn(canvas, 87.5, doc.height -40 + doc.topMargin-93)

       

        # FOOTER

        footer4 = Paragraph("Fecha: "+str(fecha),ta_l7)
        w, h = footer4.wrap(doc.width -200, doc.bottomMargin) 
        footer4.drawOn(canvas,doc.height-105, doc.topMargin +620, h)

        canvas.restoreState()

def AliementoPDF(request):
    response = HttpResponse(content_type='application/pdf')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer)
    doc = SimpleDocTemplate(buffer,
        pagesizes=letter,
        rightMargin = 30,
        leftMargin= 30,
        topMargin=176.9,
        bottomMargin=50,
        paginate_by=0,
    )
    ta_r = ParagraphStyle('parrafos', 
        alignment = TA_RIGHT,
        fontSize = 13,
        fontName="Helvetica-Bold",
        )
    persona=[]
    styles = getSampleStyleSheet()
    # TABLA NUMERO 1
    count = 0
    data = []
    c = Alimento.objects.all().order_by('-id')
    if c:
        for i in c:
            count =count+1 
            data.append([count,i.nombre.capitalize(),i.tipo,i.peso])
    else:
        return redirect('camada:alimento_list')
    print(data)
    x = Table(data, colWidths = [35,155,150,80])
    x.setStyle(TableStyle(
        [   
            ('GRID', (0, 0), (12, -1), 1, colors.black),
            ('ALIGN',(0,0),(3,-1),'CENTER'),
            ('FONTSIZE', (0, 0), (2, -1), 8),   
        ]
    ))
    persona.append(x)

    doc.build(persona,onFirstPage=HeaderFooterAlimento,onLaterPages=HeaderFooterAlimento,canvasmaker=NumberedCanvas)
    response.write(buffer.getvalue())
    buffer.close()

    return response

# PDF de Alimentos -----------------------------------------------------------------------------
def HeaderFooterEspecie(canvas,doc):
        canvas.saveState()
        title = "Reporte de Especies"
        canvas.setTitle(title)
        
        Story=[]
        styles = getSampleStyleSheet()
        
        archivo_imagen = finders.find('base/assets/img/cintillo.png')
        canvas.drawImage(archivo_imagen, 30, 720, width=540,height=100,preserveAspectRatio=True)

        fecha = datetime.datetime.now().strftime('%d/%m/%Y ')
        # Estilos de Párrafos
        ta_c = ParagraphStyle('parrafos', 
                            alignment = TA_CENTER,
                            fontSize = 9,
                            fontName="Helvetica",
                            )   
        ta_l = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )
        ta_l7 = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 7,
                            fontName="Helvetica-Bold",
                            )
        ta_r = ParagraphStyle('parrafos', 
                            alignment = TA_RIGHT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )

        # Header
        header = Paragraph("Listado de Especies ",ta_l,)
        w,h = header.wrap(doc.width+250, doc.topMargin)
        header.drawOn(canvas, 215, doc.height -10 + doc.topMargin-80)

        #header1 = Paragraph("<u>DIRECCIÓN DE AGRICULTURA Y TIERRA</u> ",ta_r,)
        #w,h = header1.wrap(doc.width-115, doc.topMargin)
        #header1.drawOn(canvas, 140, doc.height -10 + doc.topMargin - 2)
        
        P1 = Paragraph('''N°''',ta_c)
        P2 = Paragraph('''Nombre''',ta_c)
        P3 = Paragraph('''Taza de Mortalidad''',ta_c)
        P4 = Paragraph('''Taza de Supervivencia''',ta_c)
        P5 = Paragraph('''Cantidad de ovas''',ta_c)
        P6 = Paragraph('''Precio unitario''',ta_c)
        data = [[P1, P2, P3, P4,P5,P6]]
        header2 = Table(data, colWidths = [35,135,80,80,80,80])
        header2.setStyle(TableStyle( 
            [   
                ('GRID', (0, -1), (-1, -1), 1, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), '#28cde0'),
                ('ALIGN', (0, 0), (-1, -3), "CENTER"),
            ]
            ))
        w,h = header2.wrap(doc.width-115, doc.topMargin)
        header2.drawOn(canvas, 52.5, doc.height -40 + doc.topMargin-93)

       

        # FOOTER

        footer4 = Paragraph("Fecha: "+str(fecha),ta_l7)
        w, h = footer4.wrap(doc.width -200, doc.bottomMargin) 
        footer4.drawOn(canvas,doc.height-105, doc.topMargin +620, h)

        canvas.restoreState()

def EspeciePDF(request):
    response = HttpResponse(content_type='application/pdf')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer)
    doc = SimpleDocTemplate(buffer,
        pagesizes=letter,
        rightMargin = 30,
        leftMargin= 30,
        topMargin=176.9,
        bottomMargin=50,
        paginate_by=0,
    )
    ta_r = ParagraphStyle('parrafos', 
        alignment = TA_RIGHT,
        fontSize = 13,
        fontName="Helvetica-Bold",
        )
    persona=[]
    styles = getSampleStyleSheet()
    # TABLA NUMERO 1
    count = 0
    data = []
    c = Especie.objects.all().order_by('-id')
    if c:
        for i in c:
            count =count+1 
            data.append([count,i.nombre.capitalize(),i.taza_m,i.taza_s,i.ovas,i.precio])
    else:
        return redirect('camada:especie_list')
    print(data)
    x = Table(data, colWidths = [35,135,80,80,80,80])
    x.setStyle(TableStyle(
        [   
            ('GRID', (0, 0), (12, -1), 1, colors.black),
            ('ALIGN',(0,0),(3,-1),'CENTER'),
            ('FONTSIZE', (0, 0), (2, -1), 8),   
        ]
    ))
    persona.append(x)

    doc.build(persona,onFirstPage=HeaderFooterEspecie,onLaterPages=HeaderFooterEspecie,canvasmaker=NumberedCanvas)
    response.write(buffer.getvalue())
    buffer.close()

    return response

# PDF de Alimentos -----------------------------------------------------------------------------
def HeaderFooterProductor(canvas,doc):
        canvas.saveState()
        title = "Reporte de Productores"
        canvas.setTitle(title)
        
        Story=[]
        styles = getSampleStyleSheet()
        
        archivo_imagen = finders.find('base/assets/img/cintillo.png')
        canvas.drawImage(archivo_imagen, 30, 720, width=540,height=100,preserveAspectRatio=True)

        fecha = datetime.datetime.now().strftime('%d/%m/%Y ')
        # Estilos de Párrafos
        ta_c = ParagraphStyle('parrafos', 
                            alignment = TA_CENTER,
                            fontSize = 9,
                            fontName="Helvetica",
                            )   
        ta_l = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )
        ta_l1 = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 9,
                            fontName="Helvetica",
                            )
        ta_l7 = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 7,
                            fontName="Helvetica-Bold",
                            )
        ta_r = ParagraphStyle('parrafos', 
                            alignment = TA_RIGHT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )

        # Header
        header = Paragraph("Listado de Productores ",ta_l)
        w,h = header.wrap(doc.width+250, doc.topMargin)
        header.drawOn(canvas, 215, doc.height -10 + doc.topMargin-80)
        if estado:
            header1 = Paragraph("(Estado: "+estado+")",ta_l1)
            w,h = header1.wrap(doc.width+250, doc.topMargin)
            header1.drawOn(canvas, 445, doc.height -10 + doc.topMargin-85)

        #header1 = Paragraph("<u>DIRECCIÓN DE AGRICULTURA Y TIERRA</u> ",ta_r,)
        #w,h = header1.wrap(doc.width-115, doc.topMargin)
        #header1.drawOn(canvas, 140, doc.height -10 + doc.topMargin - 2)
        
        P1 = Paragraph('''N°''',ta_c)
        P2 = Paragraph('''Cedula''',ta_c)
        P3 = Paragraph('''Nombre y Apellido''',ta_c)
        P5 = Paragraph('''Estado''',ta_c)
        P6 = Paragraph('''Correo''',ta_c)
        P7 = Paragraph('''Estatus''',ta_c)
        data = [[P1, P2, P3,P5,P6,P7]]
        header2 = Table(data, colWidths = [25,60,80,80,185,60])
        header2.setStyle(TableStyle( 
            [   
                ('GRID', (0, -1), (-1, -1), 1, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), '#28cde0'),
                ('ALIGN', (0, 0), (-1, -3), "CENTER"),
            ]
            ))
        w,h = header2.wrap(doc.width-115, doc.topMargin)
        header2.drawOn(canvas, 52.5, doc.height -40 + doc.topMargin-93)

       

        # FOOTER

        footer4 = Paragraph("Fecha: "+str(fecha),ta_l7)
        w, h = footer4.wrap(doc.width -200, doc.bottomMargin) 
        footer4.drawOn(canvas,doc.height-105, doc.topMargin +620, h)

        canvas.restoreState()

def ProductorPDF(request):
    response = HttpResponse(content_type='application/pdf')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer)
    doc = SimpleDocTemplate(buffer,
        pagesizes=letter,
        rightMargin = 30,
        leftMargin= 30,
        topMargin=176.9,
        bottomMargin=50,
        paginate_by=0,
    )
    ta_r = ParagraphStyle('parrafos', 
        alignment = TA_CENTER,
        fontSize = 9,
        fontName="Helvetica-bold",
        )
    persona=[]
    styles = getSampleStyleSheet()
    
    # TABLA NUMERO 1
    global count
    count = 0
    data = []
    global estado
    filtro = User.objects.filter(is_productor=True).order_by('-id')
    if(request.GET['estado']):
        estado = request.GET['estado']
        c = filtro.filter(estados=estado)

    else:
        estado = ''
        c = filtro
    if c:
        for i in c:
            count =count+1 
            if i.is_active:
                activo ='Activo'
            else:
                activo ='Inactivo'
            data.append([count,i.cedula,i.first_name.capitalize()+' '+i.last_name.capitalize(),i.estados,i.email,activo])
    else:
        return redirect('camada:productor_list')
    x = Table(data, colWidths = [25,60,80,80,185,60])
    x.setStyle(TableStyle(
        [   
            ('GRID', (0, 0), (12, -1), 1, colors.black),
            ('ALIGN',(0,0),(3,-1),'CENTER'),
            ('FONTSIZE', (0, 0), (2, -1), 8), 
        ]
    ))


    persona.append(x)

    

    doc.build(persona,onFirstPage=HeaderFooterProductor,onLaterPages=HeaderFooterProductor,canvasmaker=NumberedCanvas)
    response.write(buffer.getvalue())
    buffer.close()

    return response

# PDF de Alimentos -----------------------------------------------------------------------------
def HeaderFooterCamada_f(canvas,doc):
        canvas.saveState()
        title = "Reporte de Camada"
        canvas.setTitle(title)
        
        Story=[]
        styles = getSampleStyleSheet()
        
        archivo_imagen = finders.find('base/assets/img/cintillo.png')
        canvas.drawImage(archivo_imagen, 30, 720, width=540,height=100,preserveAspectRatio=True)

        fecha = datetime.datetime.now().strftime('%d/%m/%Y ')
        # Estilos de Párrafos
        ta_c = ParagraphStyle('parrafos', 
                            alignment = TA_CENTER,
                            fontSize = 9,
                            fontName="Helvetica",
                            )   
        ta_l = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )
        ta_l7 = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 7,
                            fontName="Helvetica-Bold",
                            )
        ta_r = ParagraphStyle('parrafos', 
                            alignment = TA_RIGHT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )

        # Header
        header = Paragraph("Listado de Desoves en formación ",ta_l,)
        w,h = header.wrap(doc.width+250, doc.topMargin)
        header.drawOn(canvas, 215, doc.height -10 + doc.topMargin-80)

        #header1 = Paragraph("<u>DIRECCIÓN DE AGRICULTURA Y TIERRA</u> ",ta_r,)
        #w,h = header1.wrap(doc.width-115, doc.topMargin)
        #header1.drawOn(canvas, 140, doc.height -10 + doc.topMargin - 2)
        
        P1 = Paragraph('''N°''',ta_c)
        P2 = Paragraph('''Especie''',ta_c)
        P3 = Paragraph('''Fecha''',ta_c)
        P4 = Paragraph('''Ejemplares hembras''',ta_c)
        P5 = Paragraph('''Ejemplares machos''',ta_c)
        P6 = Paragraph('''Ejemplares Esperados''',ta_c)
        P7 = Paragraph('''Estanque''',ta_c)
        P8 = Paragraph('''Alimento''',ta_c)
        data = [[P1, P2, P3, P4,P5,P6,P7,P8]]
        header2 = Table(data, colWidths = [35,80,60,60,60,100,60,60])
        header2.setStyle(TableStyle( 
            [   
                ('GRID', (0, -1), (-1, -1), 1, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), '#28cde0'),
                ('ALIGN', (0, 0), (-1, -3), "CENTER"),
            ]
            ))
        w,h = header2.wrap(doc.width-115, doc.topMargin)
        header2.drawOn(canvas, 40, doc.height -40 + doc.topMargin-93)

       

        # FOOTER

        footer4 = Paragraph("Fecha: "+str(fecha),ta_l7)
        w, h = footer4.wrap(doc.width -200, doc.bottomMargin) 
        footer4.drawOn(canvas,doc.height-105, doc.topMargin +620, h)

        canvas.restoreState()

def Camada_fPDF(request):
    response = HttpResponse(content_type='application/pdf')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer)
    doc = SimpleDocTemplate(buffer,
        pagesizes=letter,
        rightMargin = 30,
        leftMargin= 30,
        topMargin=176.9,
        bottomMargin=50,
        paginate_by=0,
    )
    ta_r = ParagraphStyle('parrafos', 
        alignment = TA_RIGHT,
        fontSize = 13,
        fontName="Helvetica-Bold",
        )
    persona=[]
    styles = getSampleStyleSheet()
    # TABLA NUMERO 1
    count = 0
    data = []
    c = Camada.objects.filter(estatus=1).order_by('-id')
    if c:
        for i in c:
            count =count+1 
            data.append([count,i.especie.nombre.capitalize(),i.fecha,i.cantidad_h,i.cantidad_m,i.cantidad_e,i.estanque,i.alimento.nombre.capitalize()])
    else:
        return redirect('camada:camada_list')
    print(data)
    x = Table(data, colWidths = [35,80,60,60,60,100,60,60])
    x.setStyle(TableStyle(
        [   
            ('GRID', (0, 0), (12, -1), 1, colors.black),
            ('ALIGN',(0,0),(3,-1),'CENTER'),
            ('FONTSIZE', (0, 0), (2, -1), 8),   
        ]
    ))
    persona.append(x)

    doc.build(persona,onFirstPage=HeaderFooterCamada_f,onLaterPages=HeaderFooterCamada_f,canvasmaker=NumberedCanvas)
    response.write(buffer.getvalue())
    buffer.close()

    return response

# PDF de Alimentos -----------------------------------------------------------------------------
def HeaderFooterCamada_g(canvas,doc):
        canvas.saveState()
        title = "Reporte de Camada"
        canvas.setTitle(title)
        
        Story=[]
        styles = getSampleStyleSheet()
        
        archivo_imagen = finders.find('base/assets/img/cintillo.png')
        canvas.drawImage(archivo_imagen, 30, 720, width=540,height=100,preserveAspectRatio=True)

        fecha = datetime.datetime.now().strftime('%d/%m/%Y ')
        # Estilos de Párrafos
        ta_c = ParagraphStyle('parrafos', 
                            alignment = TA_CENTER,
                            fontSize = 9,
                            fontName="Helvetica",
                            )   
        ta_l = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )
        ta_l7 = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 7,
                            fontName="Helvetica-Bold",
                            )
        ta_r = ParagraphStyle('parrafos', 
                            alignment = TA_RIGHT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )

        # Header
        header = Paragraph("Listado de Desoves en gestación ",ta_l,)
        w,h = header.wrap(doc.width+250, doc.topMargin)
        header.drawOn(canvas, 215, doc.height -10 + doc.topMargin-80)

        #header1 = Paragraph("<u>DIRECCIÓN DE AGRICULTURA Y TIERRA</u> ",ta_r,)
        #w,h = header1.wrap(doc.width-115, doc.topMargin)
        #header1.drawOn(canvas, 140, doc.height -10 + doc.topMargin - 2)
        
        P1 = Paragraph('''N°''',ta_c)
        P2 = Paragraph('''Especie''',ta_c)
        P3 = Paragraph('''Fecha''',ta_c)
        P4 = Paragraph('''Ejemplares hembras''',ta_c)
        P5 = Paragraph('''Ejemplares machos''',ta_c)
        P6 = Paragraph('''Ejemplares Esperados''',ta_c)
        P7 = Paragraph('''Estanque''',ta_c)
        P8 = Paragraph('''Alimento''',ta_c)
        data = [[P1, P2, P3, P4,P5,P6,P7,P8]]
        header2 = Table(data, colWidths = [35,80,60,60,60,100,60,60])
        header2.setStyle(TableStyle( 
            [   
                ('GRID', (0, -1), (-1, -1), 1, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), '#28cde0'),
                ('ALIGN', (0, 0), (-1, -3), "CENTER"),
            ]
            ))
        w,h = header2.wrap(doc.width-115, doc.topMargin)
        header2.drawOn(canvas, 40, doc.height -40 + doc.topMargin-93)

       

        # FOOTER

        footer4 = Paragraph("Fecha: "+str(fecha),ta_l7)
        w, h = footer4.wrap(doc.width -200, doc.bottomMargin) 
        footer4.drawOn(canvas,doc.height-105, doc.topMargin +620, h)

        canvas.restoreState()

def Camada_gPDF(request):
    response = HttpResponse(content_type='application/pdf')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer)
    doc = SimpleDocTemplate(buffer,
        pagesizes=letter,
        rightMargin = 30,
        leftMargin= 30,
        topMargin=176.9,
        bottomMargin=50,
        paginate_by=0,
    )
    ta_r = ParagraphStyle('parrafos', 
        alignment = TA_RIGHT,
        fontSize = 13,
        fontName="Helvetica-Bold",
        )
    persona=[]
    styles = getSampleStyleSheet()
    # TABLA NUMERO 1
    count = 0
    data = []
    c = Camada.objects.filter(estatus=2).order_by('-id')
    if c:
        for i in c:
            count =count+1 
            data.append([count,i.especie.nombre.capitalize(),i.fecha,i.cantidad_h,i.cantidad_m,i.cantidad_e,i.estanque,i.alimento.nombre.capitalize()])
    else:
        return redirect('camada:camada_list')
    print(data)
    x = Table(data, colWidths = [35,80,60,60,60,100,60,60])
    x.setStyle(TableStyle(
        [   
            ('GRID', (0, 0), (12, -1), 1, colors.black),
            ('ALIGN',(0,0),(3,-1),'CENTER'),
            ('FONTSIZE', (0, 0), (2, -1), 8),   
        ]
    ))
    persona.append(x)

    doc.build(persona,onFirstPage=HeaderFooterCamada_g,onLaterPages=HeaderFooterCamada_g,canvasmaker=NumberedCanvas)
    response.write(buffer.getvalue())
    buffer.close()

    return response

# PDF de Alimentos -----------------------------------------------------------------------------
def HeaderFooterCamada_l(canvas,doc):
        canvas.saveState()
        title = "Reporte de Camada"
        canvas.setTitle(title)
        
        Story=[]
        styles = getSampleStyleSheet()
        
        archivo_imagen = finders.find('base/assets/img/cintillo.png')
        canvas.drawImage(archivo_imagen, 30, 720, width=540,height=100,preserveAspectRatio=True)

        fecha = datetime.datetime.now().strftime('%d/%m/%Y ')
        # Estilos de Párrafos
        ta_c = ParagraphStyle('parrafos', 
                            alignment = TA_CENTER,
                            fontSize = 9,
                            fontName="Helvetica",
                            )   
        ta_l = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )
        ta_l7 = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 7,
                            fontName="Helvetica-Bold",
                            )
        ta_r = ParagraphStyle('parrafos', 
                            alignment = TA_RIGHT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )

        # Header
        header = Paragraph("Listado de Desoves Listas ",ta_l,)
        w,h = header.wrap(doc.width+250, doc.topMargin)
        header.drawOn(canvas, 215, doc.height -10 + doc.topMargin-80)

        #header1 = Paragraph("<u>DIRECCIÓN DE AGRICULTURA Y TIERRA</u> ",ta_r,)
        #w,h = header1.wrap(doc.width-115, doc.topMargin)
        #header1.drawOn(canvas, 140, doc.height -10 + doc.topMargin - 2)
        
        P1 = Paragraph('''N°''',ta_c)
        P2 = Paragraph('''Especie''',ta_c)
        P3 = Paragraph('''Fecha''',ta_c)
        P4 = Paragraph('''Ejemplares hembras''',ta_c)
        P5 = Paragraph('''Ejemplares machos''',ta_c)
        P6 = Paragraph('''Ejemplares Esperados''',ta_c)
        P7 = Paragraph('''Estanque''',ta_c)
        P8 = Paragraph('''Alimento''',ta_c)
        data = [[P1, P2, P3, P4,P5,P6,P7,P8]]
        header2 = Table(data, colWidths = [35,80,60,60,60,100,60,60])
        header2.setStyle(TableStyle( 
            [   
                ('GRID', (0, -1), (-1, -1), 1, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), '#28cde0'),
                ('ALIGN', (0, 0), (-1, -3), "CENTER"),
            ]
            ))
        w,h = header2.wrap(doc.width-115, doc.topMargin)
        header2.drawOn(canvas, 40, doc.height -40 + doc.topMargin-93)

       

        # FOOTER

        footer4 = Paragraph("Fecha: "+str(fecha),ta_l7)
        w, h = footer4.wrap(doc.width -200, doc.bottomMargin) 
        footer4.drawOn(canvas,doc.height-105, doc.topMargin +620, h)

        canvas.restoreState()

def Camada_lPDF(request):
    response = HttpResponse(content_type='application/pdf')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer)
    doc = SimpleDocTemplate(buffer,
        pagesizes=letter,
        rightMargin = 30,
        leftMargin= 30,
        topMargin=176.9,
        bottomMargin=50,
        paginate_by=0,
    )
    ta_r = ParagraphStyle('parrafos', 
        alignment = TA_RIGHT,
        fontSize = 13,
        fontName="Helvetica-Bold",
        )
    persona=[]
    styles = getSampleStyleSheet()
    # TABLA NUMERO 1
    count = 0
    data = []
    c = Camada.objects.filter(estatus=3).order_by('-id')
    if c:
        for i in c:
            count =count+1 
            data.append([count,i.especie.nombre.capitalize(),i.fecha,i.cantidad_h,i.cantidad_m,i.cantidad_e,i.estanque,i.alimento.nombre.capitalize()])
    else:
        return redirect('camada:camada_list')
    print(data)
    x = Table(data, colWidths = [35,80,60,60,60,100,60,60])
    x.setStyle(TableStyle(
        [   
            ('GRID', (0, 0), (12, -1), 1, colors.black),
            ('ALIGN',(0,0),(3,-1),'CENTER'),
            ('FONTSIZE', (0, 0), (2, -1), 8),   
        ]
    ))
    persona.append(x)

    doc.build(persona,onFirstPage=HeaderFooterCamada_l,onLaterPages=HeaderFooterCamada_l,canvasmaker=NumberedCanvas)
    response.write(buffer.getvalue())
    buffer.close()

    return response

# PDF de Alimentos -----------------------------------------------------------------------------
def HeaderFooterCamada_p(canvas,doc):
        canvas.saveState()
        title = "Reporte de Camada"
        canvas.setTitle(title)
        
        Story=[]
        styles = getSampleStyleSheet()
        
        archivo_imagen = finders.find('base/assets/img/cintillo.png')
        canvas.drawImage(archivo_imagen, 30, 720, width=540,height=100,preserveAspectRatio=True)

        fecha = datetime.datetime.now().strftime('%d/%m/%Y ')
        # Estilos de Párrafos
        ta_c = ParagraphStyle('parrafos', 
                            alignment = TA_CENTER,
                            fontSize = 9,
                            fontName="Helvetica",
                            )   
        ta_l = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )
        ta_l7 = ParagraphStyle('parrafos', 
                            alignment = TA_LEFT,
                            fontSize = 7,
                            fontName="Helvetica-Bold",
                            )
        ta_r = ParagraphStyle('parrafos', 
                            alignment = TA_RIGHT,
                            fontSize = 13,
                            fontName="Helvetica-Bold",
                            )

        # Header
        header = Paragraph("Listado de Desoves Perdidas ",ta_l,)
        w,h = header.wrap(doc.width+250, doc.topMargin)
        header.drawOn(canvas, 215, doc.height -10 + doc.topMargin-80)

        #header1 = Paragraph("<u>DIRECCIÓN DE AGRICULTURA Y TIERRA</u> ",ta_r,)
        #w,h = header1.wrap(doc.width-115, doc.topMargin)
        #header1.drawOn(canvas, 140, doc.height -10 + doc.topMargin - 2)
        
        P1 = Paragraph('''N°''',ta_c)
        P2 = Paragraph('''Especie''',ta_c)
        P3 = Paragraph('''Fecha''',ta_c)
        P4 = Paragraph('''Ejemplares hembras''',ta_c)
        P5 = Paragraph('''Ejemplares machos''',ta_c)
        P6 = Paragraph('''Ejemplares Esperados''',ta_c)
        P7 = Paragraph('''Estanque''',ta_c)
        P8 = Paragraph('''Alimento''',ta_c)
        data = [[P1, P2, P3, P4,P5,P6,P7,P8]]
        header2 = Table(data, colWidths = [35,80,60,60,60,100,60,60])
        header2.setStyle(TableStyle( 
            [   
                ('GRID', (0, -1), (-1, -1), 1, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), '#28cde0'),
                ('ALIGN', (0, 0), (-1, -3), "CENTER"),
            ]
            ))
        w,h = header2.wrap(doc.width-115, doc.topMargin)
        header2.drawOn(canvas, 40, doc.height -40 + doc.topMargin-93)

       

        # FOOTER

        footer4 = Paragraph("Fecha: "+str(fecha),ta_l7)
        w, h = footer4.wrap(doc.width -200, doc.bottomMargin) 
        footer4.drawOn(canvas,doc.height-105, doc.topMargin +620, h)

        canvas.restoreState()

def Camada_pPDF(request):
    response = HttpResponse(content_type='application/pdf')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer)
    doc = SimpleDocTemplate(buffer,
        pagesizes=letter,
        rightMargin = 30,
        leftMargin= 30,
        topMargin=176.9,
        bottomMargin=50,
        paginate_by=0,
    )
    ta_r = ParagraphStyle('parrafos', 
        alignment = TA_RIGHT,
        fontSize = 13,
        fontName="Helvetica-Bold",
        )
    persona=[]
    styles = getSampleStyleSheet()
    # TABLA NUMERO 1
    count = 0
    data = []
    c = Camada.objects.filter(estatus=4).order_by('-id')
    if c:
        for i in c:
            count =count+1 
            data.append([count,i.especie.nombre.capitalize(),i.fecha,i.cantidad_h,i.cantidad_m,i.cantidad_e,i.estanque,i.alimento.nombre.capitalize()])
    else:
        return redirect('camada:camada_list')
    print(data)
    x = Table(data, colWidths = [35,80,60,60,60,100,60,60])
    x.setStyle(TableStyle(
        [   
            ('GRID', (0, 0), (12, -1), 1, colors.black),
            ('ALIGN',(0,0),(3,-1),'CENTER'),
            ('FONTSIZE', (0, 0), (2, -1), 8),   
        ]
    ))
    persona.append(x)

    doc.build(persona,onFirstPage=HeaderFooterCamada_p,onLaterPages=HeaderFooterCamada_p,canvasmaker=NumberedCanvas)
    response.write(buffer.getvalue())
    buffer.close()

    return response

#Estadisticas de Especies
class EstadisticaEspecie(TemplateView):
    def get(self,request):
        today=datetime.datetime.now()
        if len(str(today.month)) == 1:
            mes = '0'+str(today.month)
        else:
            mes = today.month

        inicio="%s-%s-01" % (today.year, mes)
        fin="%s-%s-%s" % (today.year, mes, calendar.monthrange(today.year-1, int(mes))[1])
        especie = Especie.objects.all()
        estadistica = []
        est = []
        
        for i in especie:
            lista = Alevinaje.objects.filter(fecha__range=[inicio,fin],estatus=3,camada__especie=i).count()
            if lista >0:    
                est.append(
                    {
                    "nombre":i.nombre.capitalize(),
                    "count":lista
                })
        if est:
            found = "True"
            estadistica.append(est)
        else:
            found = "False"
        estadistica.append({
            "found":found
        })
        print(estadistica)
        return render(request,'panel/reporte/estadistica_especie.html',{"estadistica":estadistica})
    def post(self,request):
        mes = request.POST['mes']
        ano = request.POST['ano']
        inicio="%s-%s-01" % (ano, mes)
        fin="%s-%s-%s" % (int(ano), mes, calendar.monthrange(int(ano)-1, int(mes))[1])

        especie = Especie.objects.all()
        estadistica = []
        est = []
        
        for i in especie:
            lista = Alevinaje.objects.filter(fecha__range=[inicio,fin],estatus=3,camada__especie=i).count()
            if lista >0:    
                est.append(
                    {
                    "nombre":i.nombre.capitalize(),
                    "count":lista
                })
        print(est)
        if est:
            found = "True"
            estadistica.append(est)
        else:
            found = "False"
        estadistica.append({
            "found":found
        })
        return JsonResponse(estadistica,safe=False)

#Estadisticas por estado
class EstadisticaEstado(TemplateView):
    def get(self,request):
        today=datetime.datetime.now()
        if len(str(today.month)) == 1:
            mes = '0'+str(today.month)
        else:
            mes = today.month

        inicio="%s-%s-01" % (today.year, mes)
        fin="%s-%s-%s" % (today.year, mes, calendar.monthrange(today.year-1, int(mes))[1])
        productor = User.objects.filter(is_productor=True)
        estadistica = []
        est = []
        
        for i in productor:
            lista = Venta.objects.filter(fecha__range=[inicio,fin],estatus="Procesada",productor=i).count()
            if lista >0:    
                est.append(
                    {
                    "nombre":i.estados.capitalize(),
                    "count":lista
                })
        if est:
            found = "True"
            estadistica.append(est)
        else:
            found = "False"
        estadistica.append({
            "found":found
        })
        print(estadistica)
        return render(request,'panel/reporte/estadistica_estado.html',{"estadistica":estadistica})
    def post(self,request):
        mes = request.POST['mes']
        ano = request.POST['ano']
        inicio="%s-%s-01" % (ano, mes)
        fin="%s-%s-%s" % (int(ano), mes, calendar.monthrange(int(ano)-1, int(mes))[1])

        productor = User.objects.filter(is_productor=True)
        estadistica = []
        est = []
        
        for i in productor:
            lista = Venta.objects.filter(fecha__range=[inicio,fin],estatus="Procesada",productor=i).count()
            if lista >0:    
                est.append(
                    {
                    "nombre":i.estados.capitalize(),
                    "count":lista
                })
        if est:
            found = "True"
            estadistica.append(est)
        else:
            found = "False"
        estadistica.append({
            "found":found
        })
        return JsonResponse(estadistica,safe=False)
from apps.core.models import Notificacion

def notificacion(request):
	if request.user.is_authenticated():
		if request.user.is_administrador:
			notificacion = Notificacion.objects.filter(tipo="productor").order_by('-id')[:4]
			pendientes = Notificacion.objects.filter(tipo="productor",estatus=False).order_by('-id')[:4]
			pen = len(pendientes)
		
		elif request.user.is_productor:
			notificacion = Notificacion.objects.filter(usuario=request.user,tipo="administrador").order_by('-id')[:4]
			pendientes = Notificacion.objects.filter(usuario=request.user,tipo="administrador",estatus=False).order_by('-id')[:4]
			pen = len(pendientes)
		elif request.user.is_superuser:
			notificacion = ''
			pen = ''

		else:
			notificacion = ''
			pen = ''
	else:
		notificacion = ''
		pen = ''
	return {'notificaciones':notificacion,'pendientes':pen}
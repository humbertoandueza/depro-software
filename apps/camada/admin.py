from django.contrib import admin

# Register your models here.
from .models import *
admin.site.register(Especie)
admin.site.register(Alimento)
admin.site.register(Estanque)
admin.site.register(Estatus)
admin.site.register(Camada)
admin.site.register(Formula)


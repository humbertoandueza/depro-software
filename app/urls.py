"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.contrib.auth.views import login
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #app de usuarios
    url(r'^', include('apps.core.urls',namespace="core")),
    #app camada
    url(r'^camada/', include('apps.camada.urls',namespace="camada")),
    
    #app alevinaje
    url(r'^alevinaje/', include('apps.alevinaje.urls',namespace="alevinaje")),
    
    #app ventas
    url(r'^productor/', include('apps.ventas.urls',namespace="ventas")),

    #app reporte
    url(r'^reporte/', include('apps.reporte.urls',namespace="reporte")),


    #login y autenticacion
    url(r'^accounts/login/',login ,{'redirect_authenticated_user': True}, name='login'),
    url(r'^accounts/',include('django.contrib.auth.urls')),
]
